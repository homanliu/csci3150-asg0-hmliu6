/***********************************
* Program code for count()
***********************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "util.h"

void count(int argc,char *argv[])
{
	/* Write Your Code HERE! */
	int line = 0, word = 0, ch = 0; 
	bool enter = false;
	char c;
	FILE *fp;
	if(argc != 2){
		printf("Wrong Number of Argument!\n");
                return;
	}
	fp = fopen(argv[1], "r");
	if(!fp){
		printf("Cannot open the file!\n");
		return;
	}
	c = getc(fp);
	if(c != EOF){
		word = 1;
	}
	while(c != EOF){
		ch += 1;
		if(c == '\n'){
			enter = true;
			line += 1;
		}
		else if(c == ' '){
			word += 1;
		}
		else if(c != '\n' && enter){
			word += 1;
			enter = false;
		}
		c = getc(fp);
	}
	printf("%d %d %d %s\n",line, word, ch, argv[1]);
}
